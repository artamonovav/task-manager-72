package ru.t1.artamonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.artamonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.artamonov.tm.comparator.NameComparator;
import ru.t1.artamonov.tm.dto.model.ProjectDTO;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.artamonov.tm.exception.field.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public class ProjectDTOService implements IProjectDTOService {

    @NotNull
    @Autowired
    private IProjectDTORepository projectRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO add(@Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO add(@Nullable final String userId, @Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        model.setUserId(userId);
        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<ProjectDTO> add(@NotNull final Collection<ProjectDTO> models) {
        if (models == null) throw new ProjectNotFoundException();
        projectRepository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        return update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        projectRepository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return projectRepository.findAllByUserIdWithSort(userId, NameComparator.INSTANCE.name());
        return projectRepository.findAllByUserIdWithSort(userId, getSortType(sort));
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Override
    public long getSize() {
        return projectRepository.count();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.countByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO remove(@Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.delete(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO remove(@Nullable final String userId, @Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, model.getId());
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final Collection<ProjectDTO> collection) {
        if (collection == null) throw new ProjectNotFoundException();
        projectRepository.deleteAll(collection);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project = findOneById(id);
        projectRepository.deleteById(id);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project = projectRepository.findByUserIdAndId(userId, id);
        projectRepository.deleteByUserIdAndId(userId, id);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> collections) {
        if (collections == null) return new ArrayList<>();
        clear();
        add(collections);
        return collections;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO update(@NotNull final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
        return project;
    }

    @NotNull
    private String getSortType(@NotNull final Sort sort) {
        if (sort == Sort.BY_NAME) return "name";
        else if (sort == Sort.BY_STATUS) return "status";
        else return "created";
    }

}
