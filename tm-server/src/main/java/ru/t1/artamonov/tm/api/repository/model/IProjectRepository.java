package ru.t1.artamonov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.model.Project;

import java.util.List;


@Repository
public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Transactional
    void deleteByUserId(String userId);

    boolean existsByUserIdAndId(String userId, String id);

    @Nullable
    List<Project> findAllByUserId(String userId);

    @Nullable
    Project findByUserIdAndId(String userId, String id);

    long countByUserId(String userId);

    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sortType")
    List<Project> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortType") String sortType);

}