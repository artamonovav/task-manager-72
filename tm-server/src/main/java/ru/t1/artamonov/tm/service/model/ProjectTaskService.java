package ru.t1.artamonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.model.IProjectRepository;
import ru.t1.artamonov.tm.api.repository.model.ITaskRepository;
import ru.t1.artamonov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.artamonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.artamonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.artamonov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.artamonov.tm.exception.field.TaskIdEmptyException;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.model.Task;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@NoArgsConstructor
public class ProjectTaskService implements IProjectTaskDTOService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();

        if (!projectRepository.existsByUserIdAndId(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findByUserIdAndId(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProject(projectRepository.findById(projectId).orElse(null));
        taskRepository.saveAndFlush(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        taskRepository.deleteByProjectId(projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsByUserIdAndId(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = taskRepository.findOneByUserIdAndId(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(projectRepository.findById(projectId).orElse(null));
        taskRepository.saveAndFlush(task);
    }

}
