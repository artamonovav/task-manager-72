package ru.t1.artamonov.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.dto.model.AbstractModelDTO;

@Repository
public interface IDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}
