package ru.t1.artamonov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.util.FormatUtil;

@Component
public final class SystemInfoListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "info";

    @NotNull
    private static final String ARGUMENT = "-i";

    @NotNull
    private static final String DESCRIPTION = "Display system info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@systemInfoListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory: " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        @NotNull final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

}
