package ru.t1.artamonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.DataBackupLoadRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Component
public final class DataBackupLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "backup-load";

    @NotNull
    private static final String DESCRIPTION = "Load backup from file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBackupLoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        domainEndpointClient.loadDataBackup(request);
    }

}
