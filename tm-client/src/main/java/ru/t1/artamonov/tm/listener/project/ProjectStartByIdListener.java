package ru.t1.artamonov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "Start project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectStartByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setProjectId(id);
        request.setStatus(Status.IN_PROGRESS);
        projectEndpointClient.changeProjectStatusById(request);
    }

}
