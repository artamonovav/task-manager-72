package ru.t1.artamonov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.UserRemoveRequest;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.util.TerminalUtil;

@Component
public final class UserRemoveListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "user-remove";

    @NotNull
    private static final String DESCRIPTION = "user remove";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userRemoveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER REMOVE]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);
        userEndpointClient.removeUser(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
