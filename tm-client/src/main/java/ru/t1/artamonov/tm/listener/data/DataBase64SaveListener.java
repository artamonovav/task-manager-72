package ru.t1.artamonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.DataBase64SaveRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Component
public final class DataBase64SaveListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-base64";

    @NotNull
    private static final String DESCRIPTION = "Save data in base64 file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64SaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE BASE64]");
        @NotNull DataBase64SaveRequest request = new DataBase64SaveRequest(getToken());
        domainEndpointClient.saveDataBase64(request);
    }

}
