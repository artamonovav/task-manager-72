package ru.t1.artamonov.tm.unit.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.artamonov.tm.config.*;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.service.ProjectService;
import ru.t1.artamonov.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectServiceTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final Project project1 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project2 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project3 = new Project(UUID.randomUUID().toString());

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.addByUserId(UserUtil.getUserId(), project1);
        projectService.addByUserId(UserUtil.getUserId(), project2);
    }

    @After
    public void cleanByUserIdTest() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void contextLoadsTest() {
        Assert.assertNotNull(authenticationManager);
        Assert.assertNotNull(projectService);
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, projectService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdAndUserId() {
        Assert.assertNotNull(projectService.findOneByIdAndUserId(project1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByIdAndUserId() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(project1);
        projectService.removeByIdAndUserId(project1.getId(), UserUtil.getUserId());
        Assert.assertNull(projectService.findOneByIdAndUserId(project1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeAllByUserId() {
        projectService.removeAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void writeList() {
        projectService.add(project3);
        Assert.assertEquals(project3.getName(), projectService.findOneById(project3.getId()).getName());
        project3.setName(UUID.randomUUID().toString());
        projectService.update(project3);
        Assert.assertEquals(project3.getName(), projectService.findOneById(project3.getId()).getName());
    }

}
