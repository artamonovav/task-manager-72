package ru.t1.artamonov.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.config.*;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.util.UserUtil;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectRepositoryTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final Project model1 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project model2 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project model3 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project model4 = new Project(UUID.randomUUID().toString());

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        model1.setUserId(UserUtil.getUserId());
        model2.setUserId(UserUtil.getUserId());
        model3.setUserId(UserUtil.getUserId());
        model4.setUserId(UserUtil.getUserId());
        projectRepository.save(model1);
        projectRepository.save(model2);
    }

    @After
    public void clean() {
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void contextLoadsTest() {
        Assert.assertNotNull(authenticationManager);
        Assert.assertNotNull(projectRepository);
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        List<Project> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), model1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteAllByUserIdTest() {
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteByUserIdAndIdTest() {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), model1.getId());
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), model1.getId()));
    }

}
