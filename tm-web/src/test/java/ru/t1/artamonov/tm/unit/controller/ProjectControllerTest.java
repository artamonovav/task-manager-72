package ru.t1.artamonov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.artamonov.tm.config.*;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.service.ProjectService;
import ru.t1.artamonov.tm.util.UserUtil;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectControllerTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final Project project1 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project2 = new Project(UUID.randomUUID().toString());

    @Autowired
    @NotNull
    private ProjectService projectService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.addByUserId(UserUtil.getUserId(), project1);
        projectService.addByUserId(UserUtil.getUserId(), project2);
    }

    @After
    public void clean() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void contextLoadsTest() {
        Assert.assertNotNull(authenticationManager);
        Assert.assertNotNull(projectService);
        Assert.assertNotNull(mockMvc);
        Assert.assertNotNull(wac);
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final List<Project> projects = projectService.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = "/project/delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(projectService.findOneByIdAndUserId(project1.getId(), UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/project/edit/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}
