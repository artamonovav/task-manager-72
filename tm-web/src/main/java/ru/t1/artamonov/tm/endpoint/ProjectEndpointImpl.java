package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.artamonov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.service.ProjectService;
import ru.t1.artamonov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.artamonov.tm.api.endpoint.IProjectRestEndpoint")
public class ProjectEndpointImpl implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.removeByUserId(project, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public @Nullable Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findOneByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.addByUserId(UserUtil.getUserId(), project);
        return project;
    }

}
